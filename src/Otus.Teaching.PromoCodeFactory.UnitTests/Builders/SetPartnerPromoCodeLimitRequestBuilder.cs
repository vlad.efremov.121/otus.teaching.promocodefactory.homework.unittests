﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class SetPartnerPromoCodeLimitRequestBuilder
    {
        private DateTime _endDate { get; set; }
        private int _limit { get; set; }

        public SetPartnerPromoCodeLimitRequestBuilder() { }

        public SetPartnerPromoCodeLimitRequestBuilder WriteEndDate(DateTime endDate)
        {
            _endDate = endDate;
            return this;
        }

        public SetPartnerPromoCodeLimitRequestBuilder WriteLimit(int limit)
        {
            _limit = limit;
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = _endDate,
                Limit = _limit
            };
        }
    }
}
