﻿ using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Namotion.Reflection;
using System.Linq;
using Microsoft.VisualStudio.TestPlatform.PlatformAbstractions.Interfaces;
using AutoFixture.Dsl;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using FluentAssertions.Common;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<TestFixture>
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly IRepository<Partner> _partnerRepository;

        public SetPartnerPromoCodeLimitAsyncTests(TestFixture testFixture)
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
            _partnerRepository = testFixture.ServiceProvider.GetRequiredService<IRepository<Partner>>();
        }

        private Partner GetPartner()
        {
            var fixture = new Fixture();
            var partner = fixture.Build<Partner>()
                .Without(t => t.PartnerLimits)
                .Create();

            partner.PartnerLimits = new List<PartnerPromoCodeLimit> {
                fixture.Build<PartnerPromoCodeLimit>()
                .With(t=> t.PartnerId, partner.Id)
                .With(t=>t.Partner, partner)
                .Create()
            };

            return partner;
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerDoesNotExist_PartnerNotFound()
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = null;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequestBuilder().Build();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            Assert.IsAssignableFrom<NotFoundResult>(result);

        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            //Arrange

            var partner = GetPartner();
            partner.IsActive = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequest();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            Assert.IsAssignableFrom<BadRequestObjectResult>(result);

        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NotExpiredLimit_NumberIssuedPromoCodesSetZero()
        {
            //Arrange
            var partner = GetPartner();
            var limit = partner.PartnerLimits.First();
            limit.CancelDate = null;
            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WriteLimit(int.MaxValue)
                .WriteEndDate(DateTime.Now)
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            Assert.True(partner.NumberIssuedPromoCodes == 0);
            Assert.NotNull(limit.CancelDate);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ExpiredLimit_NumberIssuedPromoCodesNotSetZero()
        {
            //Arrange
            var partner = GetPartner();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WriteLimit(int.MaxValue)
                .WriteEndDate(DateTime.Now)
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            Assert.True(partner.NumberIssuedPromoCodes != 0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NegativeLimit_ReturnBadRequest()
        {
            //Arrange
            var partner = GetPartner();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WriteLimit(int.MinValue)
                .WriteEndDate(DateTime.Now)
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            Assert.IsAssignableFrom<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimit_CreateNewLimit()
        {
            //Arrange
            var partner = GetPartner();
            var conroller = new PartnersController(_partnerRepository);
            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WriteLimit(int.MaxValue)
                .WriteEndDate(DateTime.Now)
                .Build();
            await _partnerRepository.AddAsync(partner);

            // Act
            await conroller.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            var result = (await _partnerRepository.GetByIdAsync(partner.Id)).PartnerLimits;

            // Assert
            Assert.Contains(result, t => t.Limit.Equals(request.Limit) && t.EndDate.Equals(request.EndDate));
        }
    }
}