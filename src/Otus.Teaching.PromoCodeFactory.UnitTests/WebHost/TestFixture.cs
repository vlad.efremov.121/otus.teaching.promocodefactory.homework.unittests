﻿using FluentAssertions.Common;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Configurations;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost
{
    public class TestFixture : IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }

        public TestFixture() 
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            var servicesCollection = new ServiceCollection();
            ServiceProvider = servicesCollection
                .AddScoped(typeof(IRepository<>), typeof(EfRepository<>))
                .AddInMemoryDB()
                .BuildServiceProvider();
            
        }
        public void Dispose()
        {
        }
    }
}
