﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Configurations
{
    public static class Configuration
    {
        public static IServiceCollection AddInMemoryDB (this IServiceCollection services) 
        {
            var servicesCollection = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            services.AddDbContext<DataContext>(opt=>
            {
                opt.UseInMemoryDatabase("InMemoryDB", builder => { });
                opt.UseInternalServiceProvider(servicesCollection);
            });
            services.AddTransient<DbContext, DataContext>();
            return services;
        }
    }
}
